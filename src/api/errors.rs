#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use serde::{Serialize, Deserialize};
use http_api_problem::HttpApiProblem;

#[derive(Debug, thiserror::Error, Serialize, Deserialize)]
pub (super) enum ErrorKind {
    #[error("INVALID_USERNAME")]
    InvalidUsername,
    #[error("INVALID_PASSWORD")]
    InvalidPassword,
    #[error("INVALID_CREDENTIALS")]
    InvalidCredentials,
    #[error("UNAUTHORIZED")]
    Unauthorized,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FieldError {
    #[serde(rename = "path")]
    pub field: String,
    pub message: String,
    #[serde(rename = "isError")]
    error: bool,
}

impl FieldError {

    pub fn new(field: &str, message: &str) -> FieldError {
        FieldError {
            field: field.to_string(),
            message: message.to_string(),
            error: true,
        }
    }
}

pub (super) fn adjust(problem: &mut HttpApiProblem, kind: ErrorKind, fields: Vec<FieldError>) {
    problem.set_value("kind".to_owned(), &kind.to_string()).ok();
    for ref field in fields {
        problem.set_value(field.field.to_owned(), field).ok();
    }
}