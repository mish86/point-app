//! Api:Services
#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use chrono::prelude::*;

use warp::{Filter, Reply, Rejection, http::StatusCode};
use cookie::CookieJar;
use crate::{combine, boxed_on_debug};

use http_api_problem::HttpApiProblem;

use super::{ApiResult, IntoApiResult, IntoJsonReply, WithStatusReply, IntoApiRejection};
use crate::auth::{User, Session, Authorization, LoginForm, NewUser, OpenUser, AuthorizationError};
use super::routes::{user, session};
use crate::http::cookies;

pub (super) fn module_routes() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    use super::routes::CONTENT_LIMIT;

    let api_path_prefix = warp::path!("api" / "auth" / ..);
    let json_body = warp::body::content_length_limit(*CONTENT_LIMIT).and(warp::body::json());

    // POST /api/auth/login
    // let api_json_basic_login = 
    //     warp::post()
    //         .and(api_path_prefix)
    //         .and(warp::path("login"))
    //         .and(warp::path::end())
    //         .and(warp::body::content_length_limit(*CONTENT_LIMIT))
    //         .and(warp::body::json())
    //         .and(session::route_get_or_new())
    //         .and(cookies::jar())
    //         .and_then(self::basic_login)
    //         ;

    // POST /api/auth/login
    let api_header_basic_login = 
        warp::post()
            .and(api_path_prefix)
            .and(warp::path("login"))
            .and(warp::path::end())
            .and(user::route_basic_get())
            .and(session::route_get_or_new())
            .and(cookies::jar())
            .and_then(self::basic_login);

    // POST /api/auth
    let api_ping = 
        warp::post()
            .and(api_path_prefix)
            .and(warp::path::end())
            .and(user::route_try_get())
            .and(session::route_try_get())
            .and(cookies::jar())
            .and_then(self::ping);

    // POST /api/auth/signup
    let api_signup = 
        warp::post()
            .and(api_path_prefix)
            .and(warp::path("signup"))
            .and(warp::path::end())
            .and(json_body)
            .and(session::route_get_or_new())
            .and(cookies::jar())
            .and_then(self::signup);

    combine!(
        api_header_basic_login,
        api_ping,
        api_signup,
    )
}

async fn basic_login(login: LoginForm, session: Session, jar: CookieJar) -> ApiResult<impl Reply> {
    let reply = match Authorization::authenticate(&login, &session) {
        Ok(mut auth) => {
            let Authorization {ref user, ref mut session} = auth;
            info!("🔑 {} is logged in {}", user, session);
            
            session.state_mut().insert("logging_time", Local::now().timestamp()).ok();

            Ok(user.public()).json_reply().ok_reply((auth, jar))
        },
        Err(error) => {
            warn!("🚫 {} failed to log in {}", login.username, session);

            Err::<User, anyhow::Error>(error).json_reply().err_reply(jar)
        },
    };

    reply
}

async fn ping(user: Option<User>, session: Option<Session>, mut jar: CookieJar) -> ApiResult<impl Reply> {
    trace!("🏓 Ping User {:?} and Session {:?}", user, session);

    match auth_validate(user, session) {
        Ok(auth) => Ok::<OpenUser, anyhow::Error>(auth.user.public())
            .json_reply()
            .ok_reply((auth, jar)),
        Err(err) => {
            
            crate::auth::Authorization::clear(&mut jar);

            Err::<OpenUser, anyhow::Error>(err)
                .json_reply()
                .err_reply(jar)
        },
    }
}

fn auth_validate(user: Option<User>, session: Option<Session>) -> anyhow::Result<Authorization> {
    let user = user.ok_or_else(||
        AuthorizationError::unauthorized("Inactive User and/or Session").into_error()
    )?;
    let session = session.ok_or_else(||
        AuthorizationError::unauthorized("Inactive User and/or Session").into_error()
    )?;
    
    let auth = Authorization { user, session };
    match auth.validate() {
        Ok(_) => Ok(auth),
        Err(err) => Err(err),
    }
}

async fn signup(mut user: NewUser, session: Session, mut jar: CookieJar) -> ApiResult<impl Reply> {
    use crate::models::FetchError;
    use diesel::result::DatabaseErrorKind;
    use super::errors::{adjust, ErrorKind, FieldError};

    user = user_validate(user).into_rejection()?;

    match User::create(&user) {
        Ok(user) => {
            let mut auth = Authorization::new(user, session);
            let Authorization {ref user, ref mut session} = auth;
            info!("🔑 {} is signed and logged in {}", user, session);
            
            session.state_mut().insert("logging_time", Local::now().timestamp()).ok();
            
            Ok::<OpenUser, anyhow::Error>(user.public())
                .json_reply()
                .with_status(warp::http::StatusCode::CREATED)
                .ok_reply((auth, jar))
        },
        Err(error) => {
            warn!("🚫 {} failed to sign in {}", user, session);

            let error = match error.downcast::<FetchError>() {
                Ok(FetchError::QueryError(diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, info))) => {
                    let message = format!("'{0}' is already occupied, please use another one", user.username);
                    anyhow::anyhow!({
                        let mut problem = HttpApiProblem::new(&message)
                            .set_status(StatusCode::BAD_REQUEST)
                            .set_detail(&info.message().to_string());
                        adjust(&mut problem, ErrorKind::InvalidUsername, vec![
                            FieldError::new("username", &message),
                        ]);
                        problem
                    })
                },
                Ok(error) => anyhow::anyhow!(error),
                Err(error) => error
            };
            
            crate::auth::Authorization::clear(&mut jar);

            Err::<OpenUser, anyhow::Error>(error)
                .json_reply()
                .err_reply(jar)
        },
    }
}

fn user_validate(user: NewUser) -> anyhow::Result<NewUser> {
    user.validate()?.hash_password()
}