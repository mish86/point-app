//! Api:Services
#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use warp::http::StatusCode;
use warp::{Filter, Reply, Rejection};
use http_api_problem::HttpApiProblem;
use anyhow::anyhow;
use crate::{combine, boxed_on_debug};

use super::routes::{session, authorization};
use crate::http::cookies;
use super::{
    AuthAndJar, 
    ApiResult, 
    IntoApiRejection, 
    IntoApiResult, 
    IntoJsonReply, 
    EmptyReply, 
    WithStatusReply,
};
use crate::models::service::{Service, NewService, ServiceFull};

pub (super) fn module_routes() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    // use super::routes::CONTENT_LIMIT;

    // let json_body = warp::body::content_length_limit(*CONTENT_LIMIT).and(warp::body::json());
    
    let api_path_prefix = warp::path!("api" / "service" / ..);

    // GET /api/service/list
    let api_list = 
        warp::get()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path("list"))
            .and(warp::path::end())
            .and_then(self::api_list)
            ;

    // POST /api/service/
    let api_create = 
        warp::post()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::end())
            .and(warp::multipart::form())
            .and_then(self::api_create)
            ;

    // GET /api/service/<i32>
    let api_get = 
        warp::get()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::param::<i32>())
            .and(warp::path::end())
            .and_then(self::api_get)
            ;

    // GET /api/service/<i32>/cover[/any]
    let api_get_cover = 
        warp::get()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::param::<i32>())
            .and(warp::path("cover"))
            // .and(warp::path::end())
            .and_then(self::api_get_cover)
            ;

    // PATCH /api/service/<i32>
    let api_patch = 
        warp::patch()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::param::<i32>())
            .and(warp::path::end())
            .and(warp::multipart::form())
            .and_then(self::api_patch)
            ;

    // PATCH /api/service/<i32>/cover
    let api_cover_patch = 
        warp::patch()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::param::<i32>())
            .and(warp::path("cover"))
            .and(warp::path::end())
            .and(warp::multipart::form())
            .and_then(self::api_cover_patch)
            ;

    // DELETE /api/service/<i32>
    let api_get_delete = 
        warp::delete()
            .and(authorization::required())
            .and(api_path_prefix)
            .and(warp::path::param::<i32>())
            .and(warp::path::end())
            .and_then(self::api_delete)
            ;

    // GET /service
    // Redirect to login page
    let unauthorized_route = 
        warp::any()
            .and(api_path_prefix)
            .and(session::route_try_get())
            .and(cookies::jar())
            .and_then(authorization::unauthorized)
            ;

    combine!(
        api_list,
        api_create,
        api_get,
        api_get_cover,
        api_get_delete,
        api_patch,
        api_cover_patch,
        unauthorized_route,
    )
}

async fn api_list(auth: AuthAndJar) -> ApiResult<impl Reply> {
    Service::list()
        .json_reply()
        .ok_reply(auth)
}

async fn api_create(auth: AuthAndJar, data: warp::multipart::FormData) -> ApiResult<impl Reply> {
    use multipart_reader::MultipartReader;

    let mut new_service = NewService::default();
    new_service.read(data).await.into_rejection()?;

    let created = Service::create(&new_service).map_err(anyhow::Error::from).into_rejection()?;

    Ok(created)
        .json_reply()
        .with_status(warp::http::StatusCode::CREATED)
        .ok_reply(auth)
}

async fn api_patch(auth: AuthAndJar, id: i32, data: warp::multipart::FormData) -> ApiResult<impl Reply> {
    use multipart_reader::MultipartReader;

    let mut service = Service::default();
    service.id = id;
    service.read(data).await.into_rejection()?;

    Service::update(&service)
        .map(|_| {
            service
        })
        .json_reply()
        .ok_reply(auth)
}

async fn api_cover_patch(auth: AuthAndJar, id: i32, data: warp::multipart::FormData) -> ApiResult<impl Reply> {
    use multipart_reader::MultipartReader;

    let mut service = ServiceFull::default();
    service.id = id;
    service.read(data).await.into_rejection()?;

    Service::update_cover(&service)
        .and_then(|_| {
            Service::get(id)
        })
        .json_reply()
        .ok_reply(auth)
}

async fn api_get(auth: AuthAndJar, id: i32) -> ApiResult<impl Reply> {
    Service::get(id)
        .json_reply()
        .ok_reply(auth)
}

async fn api_get_cover(auth: AuthAndJar, id: i32) -> ApiResult<impl Reply> {
    use super::ErrorReply;

    let service = Service::get_full(id).into_rejection()?;
    
    let cover = service.cover
        .ok_or_else(|| anyhow!(
            HttpApiProblem::new("Resource Not Found")
            .set_status(StatusCode::NOT_FOUND)
            .set_detail(format!("Cover for Service {} is empty", id))
        ));

    match cover {
        Err(error) => {
            Ok(error)
                .problem()
                .ok_reply(auth)
        },
        Ok(cover) => {
            warp::http::response::Builder::new()
                .status(warp::http::StatusCode::OK)
                .header(warp::http::header::CONTENT_TYPE, mime::IMAGE_JPEG.as_ref())
                .body::<Vec<u8>>(cover)
                .map(|response| Box::new(response) as Box::<dyn Reply>)
                .map_err(anyhow::Error::from)
                .ok_reply(auth)
        },
    }
}

async fn api_delete(auth: AuthAndJar, id: i32) -> ApiResult<impl Reply> {
    Service::delete(id)
        .no_content()
        .ok_reply(auth)
}
