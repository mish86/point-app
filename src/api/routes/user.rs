#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use std::convert::Infallible;
use warp::{Filter, Rejection};

use crate::api::{ApiResult, ApiResultSafe, IntoApiRejection};
use crate::auth::{LoginForm, AuthorizationError};
use crate::auth::{User, UserError, USER_COOKIE_KEY};

pub fn route_basic_get() -> impl Filter<Extract = (LoginForm,), Error = Rejection> + Clone {
    warp::any()
        .and(warp::header::header(&warp::http::header::AUTHORIZATION.as_str()))
        .and_then(|token: String| self::basic_auth(Some(token)))
        .map(|login: LoginForm| {
            info!("🗿 {:?} from request with basic authorization", login);
            login
        })
}

pub fn route_get() -> impl Filter<Extract = (User,), Error = Rejection> + Clone {
    warp::any()
        .and(warp::cookie::cookie(USER_COOKIE_KEY))
        .map(|uid| {
            info!("🗿 User {:?} from request", uid);
            Some(uid)
        })
        .and_then(self::get)
}

pub fn route_try_get() -> impl Filter<Extract = (Option<User>,), Error = Infallible> + Clone {
    warp::any()
        .and(warp::cookie::optional(USER_COOKIE_KEY))
        .map(|uid| {
            info!("🗿 User {:?} from request", uid);
            uid
        })
        .and_then(self::try_get)
}

async fn basic_auth(token: Option<String>) -> ApiResult<LoginForm> {
    token
        .ok_or_else(|| AuthorizationError::invalid("Credentials are not specified").into_error())
        .and_then(read_header_basic_auth)
        .into_rejection()
} 

fn read_header_basic_auth(token: String) -> anyhow::Result<LoginForm> {
    let mut token = token.split(char::is_whitespace);
    if let Some("Basic") = token.next() {
        let token = token.next().ok_or(
            AuthorizationError::invalid("Credentials are not specified").into_error()
        )?;
        let token = base64::decode(token)
            .map_err(|err| AuthorizationError::invalid(err.to_string()).into_error())?;
        let token = String::from_utf8(token)
            .map_err(|err| AuthorizationError::invalid(err.to_string()).into_error())?;

        let mut token = token.split(':');
        let username = token.next().ok_or(
            AuthorizationError::invalid("Username is not specified").into_error()
        )?.to_string();
        let password = token.next().ok_or(
            AuthorizationError::invalid("Password is not specified").into_error()
        )?.to_string();

        Ok(LoginForm { username, password })
    }
    else { Err(AuthorizationError::UnsupportedAuthorization.into_error()) }
}

async fn get(uid: Option<String>) -> ApiResult<User> {
    uid
        .ok_or(UserError::Empty.into_error())
        .map_err(|err| {
            warn!("Failed to obtain user from cookies due: {}", err);
            err
        })
        .and_then(|uid| uid.parse::<i32>().map_err(|err| 
            UserError::invalid_id(err.to_string()).into_error()
        ))
        .and_then(|uid| User::get(uid))
        .into_rejection()
}

async fn try_get(uid: Option<String>) -> ApiResultSafe<Option<User>> {
    let found = get(uid).await;
    Ok(found.ok())
}