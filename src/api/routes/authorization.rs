//! Views:Authorization
#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use lazy_static::*;
use warp::http::Uri;
use warp::{Filter, Reply, Rejection};
use cookie::CookieJar;
use crate::http::cookies;

use super::{session, user};
use crate::auth::{Session, Authorization};
use crate::api::ApiResult;

lazy_static! {
    pub (self) static ref UNAUTHORIZED_REDIRECT_URI: Uri = "/login".parse::<Uri>().unwrap();
}

pub type AuthAndJar = (Authorization, CookieJar);

pub fn required() -> impl Filter<Extract = (AuthAndJar,), Error = Rejection> + Clone {
    warp::any()
        .and(session_and_user())
        .and(cookies::jar())
        .map(|auth, jar| (auth, jar))
}

pub fn session_and_user() -> impl Filter<Extract = (Authorization,), Error = Rejection> + Clone {
    use crate::api::IntoApiRejection;

    // important! call session from request without ask to create a new one.
    session::route_get()
        .and(user::route_get())
        .and_then(|session, user| async {
            let auth = Authorization::new(user, session);
            match auth.validate() {
                Ok(_) => Ok(auth),
                Err(error) => Err(error),
            }
            .into_rejection()
        })
}

pub async fn unauthorized(session: Option<Session>, jar: CookieJar) -> ApiResult<impl Reply> {
    use crate::api::{ 
        IntoApiResult, 
        IntoJsonReply, 
        WithStatusReply,
    };
    use http_api_problem::HttpApiProblem;
    use warp::http::StatusCode;
    use crate::api::errors::{adjust, ErrorKind};

    // remove session from server store
    match session {
        None => 
            Ok({
                let mut problem = HttpApiProblem::new("Please login to access the page")
                    .set_status(StatusCode::UNAUTHORIZED)
                    .set_detail("Unauthorized access");
                adjust(&mut problem, ErrorKind::Unauthorized, vec![]);
                problem
            })
                .json_reply()
                .with_status(StatusCode::UNAUTHORIZED)
                .err_reply(jar),
        Some(_) =>
            Ok(HttpApiProblem::with_title_from_status(StatusCode::METHOD_NOT_ALLOWED))
                .json_reply()
                .with_status(StatusCode::METHOD_NOT_ALLOWED)
                .err_reply(jar),
    }
}

#[allow(dead_code)]
pub fn redirect_to_login() -> impl Reply {
    super::redirect_to(&UNAUTHORIZED_REDIRECT_URI)
}