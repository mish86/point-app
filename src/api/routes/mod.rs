pub (super) mod user;
pub (super) mod session;
pub (super) mod authorization;

use lazy_static::*;
use warp::http::Uri;
use warp::Reply;

lazy_static! {
    pub static ref CONTENT_LIMIT: u64 = 1024 * 16;
}

/// Returns a `Reply` with 303 http code
/// TODO
#[allow(dead_code)]
pub fn redirect_to(uri: &Uri) -> impl Reply {
    warp::reply::with_header(
        warp::http::StatusCode::SEE_OTHER,
        warp::http::header::LOCATION,
        uri.path(),
    )
}