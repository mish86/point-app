//! Views:Session
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use warp::{Filter, Rejection};
use anyhow::anyhow;

use crate::auth::{SID_COOKIE_KEY, Session, SessionError, SessionFactory};
use crate::api::{ApiResult, IntoApiRejection};

pub fn route_get() -> impl Filter<Extract = (Session,), Error = Rejection> + Clone {
    warp::any()
        .and(warp::cookie::cookie(SID_COOKIE_KEY))
        .map(|sid| {
            info!("📦 Session {:?} from request", sid);
            Some(sid)
        })
        .and_then(self::get)
}

pub fn route_try_get() -> impl Filter<Extract = (Option<Session>,), Error = std::convert::Infallible> + Clone {
    warp::any()
        .and(warp::cookie::optional(SID_COOKIE_KEY))
        .map(|sid| {
            info!("📦 Session {:?} from request", sid);
            sid
        })
        .and_then(self::get)
        .map(|session| Some(session))
        .or(warp::any().map(|| None))
        .unify()
}

pub fn route_get_or_new() -> impl Filter<Extract = (Session,), Error = std::convert::Infallible> + Clone {
    warp::any()
        .and(warp::cookie::optional(SID_COOKIE_KEY))
        .map(|sid| {
            info!("📦 Session {:?} from request", sid);
            sid
        })
        .and_then(self::get)
        .or(warp::any().map(|| {
            let session = Session::new();
            info!("📦 Session {} is started!", session);
            session
        }))
        .unify()
}

async fn get(sid: Option<String>) -> ApiResult<Session> {
    let sid = match sid {
        Some(sid) => sid,
        None => {
            return None
                .ok_or(anyhow!(SessionError::Empty))
                .into_rejection();
        }
    };

    let factory = SessionFactory::instance();

    Session::validate(sid).ok()
        .and_then(|sid| factory.find(&sid))
        .ok_or(anyhow!(SessionError::Empty))
        .map_err(|err| {
            warn!("Failed to find session on server: {}", err);
            err
        })
        .into_rejection()
}