#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use std::convert::Infallible;
use warp::{Filter, Reply, Rejection};
use cookie::CookieJar;
use http_api_problem::HttpApiProblem;
use crate::{combine, boxed_on_debug};

mod routes;
mod auth;
mod service;
mod errors;

pub (super) fn module_routes() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    combine!(
        auth::module_routes(),
        service::module_routes()
    )
}

pub type AuthAndJar = (crate::auth::Authorization, CookieJar);
pub type ApiResult<T> = Result<T, Rejection>;
pub type ApiResultSafe<T> = Result<T, Infallible>;

#[derive(Debug)]
pub struct ApiProblem(pub HttpApiProblem);

impl warp::reject::Reject for ApiProblem {}

pub trait IntoApiProblem<Custom> {
    fn into_problem(self) -> Result<Custom, ApiProblem> where Self: Sized;
}

pub trait IntoApiRejection<Custom> : IntoApiProblem<Custom> {
    fn into_rejection(self) -> ApiResult<Custom> where Self: Sized  {
        Self::into_problem(self).map_err(warp::reject::custom)
    }
}

impl<Custom> IntoApiProblem<Custom> for anyhow::Result<Custom> {
    fn into_problem(self) -> Result<Custom, ApiProblem>
    where Self: Sized {
        self.map_err(crate::http::errors::into_http_error)
            .map_err(|err| ApiProblem(err))
    }
}
impl<Custom> IntoApiRejection<Custom> for anyhow::Result<Custom> {}

pub trait IntoJsonReply<T>
where T: serde::Serialize + Sized
{
    type Item;
    fn json_reply(self) -> Self::Item;
}

impl<T> IntoJsonReply<T> for anyhow::Result<T>
where T: serde::Serialize + Sized
{
    type Item = anyhow::Result<warp::reply::Json>;

    fn json_reply(self) -> Self::Item {
        self.map(|reply| warp::reply::json(&reply))
    }
}

pub trait WithStatusReply<T>
where T: Reply
{
    type Item;
    fn with_status(self, status: warp::http::StatusCode) -> Self::Item;
}

impl<T> WithStatusReply<T> for anyhow::Result<T>
where T: Reply
{
    type Item = anyhow::Result<warp::reply::WithStatus<T>>;

    fn with_status(self, status: warp::http::StatusCode) -> Self::Item {
        self.map(|reply| warp::reply::with_status(reply, status))
    }
}

pub trait WithHeaderReply<T>
where T: Reply
{
    type Item;
    fn with_header<K, V>(self, name: K, value: V) -> Self::Item
    where
        warp::http::header::HeaderName: From<K>,
        warp::http::header::HeaderValue: From<V>
    ;
}

impl<T> WithHeaderReply<T> for anyhow::Result<T>
where T: Reply
{
    type Item = anyhow::Result<warp::reply::WithHeader<T>>;

    fn with_header<K, V>(self, name: K, value: V) -> Self::Item
    where
        warp::http::header::HeaderName: From<K>,
        warp::http::header::HeaderValue: From<V>,
    {
        self.map(|reply| warp::reply::with_header(reply, name, value))
    }
}

pub trait EmptyReply {
    type Item;
    fn no_content(self) -> Self::Item;
}

impl EmptyReply for anyhow::Result<()> {
    type Item = anyhow::Result<warp::http::StatusCode>;

    fn no_content(self) -> Self::Item {
        self.map(|_| warp::http::StatusCode::NO_CONTENT)
    }
}

trait WithCookiesReply<T>
where T: Reply
{
    type Item;
    fn with_cookies(self, jar: CookieJar) -> Self::Item;
}

impl<T> WithCookiesReply<T> for anyhow::Result<T>
where T: Reply
{
    type Item = anyhow::Result<Box<dyn Reply>>;

    fn with_cookies(self, jar: CookieJar) -> Self::Item {
        use warp::http::header::HeaderValue;
        use warp::http::header::SET_COOKIE;

        let values: Vec<HeaderValue> = jar.delta()
            .map(|cookie| HeaderValue::from_str(&cookie.to_string()).unwrap())
            .collect();

        self.map(|reply| {
            let mut response: warp::reply::Response = reply.into_response();
            let headers = response.headers_mut();
            for val in values {
                headers.append(SET_COOKIE, val);
            }
            for (k, v) in headers {
                debug!("🧢 {} {:#?}", k, v);
            }

            Box::new(response) as Box<dyn Reply>
        })
    }
}

trait ErrorReply {
    type Item;
    fn problem(self) -> Self::Item;
}

impl ErrorReply for anyhow::Result<anyhow::Error> {
    type Item = anyhow::Result<warp::reply::WithStatus<warp::reply::Json>>;

    fn problem(self) -> Self::Item {
        let res = self.map(crate::http::errors::into_http_error);
        match res {
            Ok(HttpApiProblem { status: Some(status), .. }) => res.json_reply().with_status(status),
            Ok(problem) => Err(anyhow::anyhow!(problem)),
            Err(error) => Err(error),
        }
    }
}

pub trait IntoApiResult<T>
where T: Reply
{
    fn ok_reply(self, auth_and_jar: AuthAndJar) -> ApiResult<Box<dyn Reply>>;
    fn err_reply(self, jar: CookieJar) -> ApiResult<Box<dyn Reply>>;
}

impl<T> IntoApiResult<T> for anyhow::Result<T>
where T: Reply
{
    fn ok_reply(self, auth_and_jar: AuthAndJar) -> ApiResult<Box<dyn Reply>> {
        let (mut auth, mut jar) = auth_and_jar;
        auth.save(&mut jar);
        self.with_cookies(jar).into_rejection()
    }

    fn err_reply(self, jar: CookieJar) -> ApiResult<Box<dyn Reply>> {
        // Do not clear authorization in auto mode.
        // It is used in error handling for 404 responses as well.
        // crate::auth::Authorization::clear(&mut jar);
        self.with_cookies(jar).into_rejection()
    }
}