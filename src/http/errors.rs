//! API errors
#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use warp::{Reply, Rejection};
use warp::http::StatusCode;
use http_api_problem::HttpApiProblem;
use anyhow;

use crate::models::FetchError;
use diesel::result::DatabaseErrorKind;
use crate::api::ApiResultSafe;

/// Converts an anyhow 'Error' into a 'HttpError'
// TODO: refactoring of if
pub fn into_http_error(error: anyhow::Error) -> HttpApiProblem {
    use crate::auth::{UserError, SessionError, AuthorizationError};

    let error = match error.downcast::<HttpApiProblem>() {
        Ok(error) => return error,
        Err(e) => e,
    };

    if let Some(error) = error.downcast_ref::<FetchError>() {
        return match error {
            FetchError::QueryError(diesel::result::Error::NotFound) => {
                HttpApiProblem::new("Resource Not Found")
                    .set_status(StatusCode::NOT_FOUND)
                    .set_detail(&error.to_string())
            },
            FetchError::QueryError(diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, info)) => {
                HttpApiProblem::new(info.message())
                    .set_status(StatusCode::BAD_REQUEST)
                    .set_detail(&error.to_string())
            },
            _ => {
                HttpApiProblem::new("Data base error.")
                    .set_status(StatusCode::INTERNAL_SERVER_ERROR)
                    .set_detail(&error.to_string())
            }
        }
    }

    if let Some(error) = error.downcast_ref::<serde_json::Error>() {
        return HttpApiProblem::with_title_from_status(StatusCode::INTERNAL_SERVER_ERROR)
            .set_detail(&error.to_string())
        ;
    }

    if let Some(error) = error.downcast_ref::<bcrypt::BcryptError>() {
        return HttpApiProblem::with_title_from_status(StatusCode::INTERNAL_SERVER_ERROR)
            .set_detail(&error.to_string())
        ;
    }

    if let Some(error) = error.downcast_ref::<UserError>() {
        return match error {
            UserError::NotFound(msg) => {
                HttpApiProblem::new(msg.to_string())
                    .set_status(StatusCode::NOT_FOUND)
                    .set_detail(&error.to_string())
            },
            _ => {
                HttpApiProblem::with_title_from_status(StatusCode::BAD_REQUEST)
                    .set_detail(&error.to_string())
            }
        };
    }

    if let Some(error) = error.downcast_ref::<SessionError>() {
        return HttpApiProblem::with_title_from_status(StatusCode::BAD_REQUEST)
            .set_detail(&error.to_string())
        ;
    }

    if let Some(error) = error.downcast_ref::<AuthorizationError>() {
        return HttpApiProblem::with_title_from_status(StatusCode::UNAUTHORIZED)
            .set_detail(&error.to_string())
        ;
    }

    warn!("Unmapped internal error occured: {:#}", error);
    HttpApiProblem::with_title_from_status(StatusCode::INTERNAL_SERVER_ERROR)
}

/// Converts a 'Rejection' into a custom warp reply when no suitable filter is found.
pub async fn unpack_problem(rejection: Rejection) -> ApiResultSafe<Box<dyn Reply>> {
    sync_unpack_problem(rejection)
}

pub fn sync_unpack_problem(rejection: Rejection) -> ApiResultSafe<Box<dyn Reply>> {
    use cookie::CookieJar;
    use crate::api::{ApiProblem, IntoApiResult, IntoJsonReply, WithStatusReply};

    warn!("Unpack Rejection {:?}", rejection);

    // unpack api problem and return json result
    let problem = if let Some(problem) = rejection.find::<ApiProblem>() {
        let ApiProblem(problem) = problem;
        problem.clone()
    }
    else if let Some(problem) = rejection.find::<HttpApiProblem>() {
        problem.clone()
    }
    else if let Some(_) = rejection.find::<warp::reject::MethodNotAllowed>() {
        HttpApiProblem::with_title_from_status(StatusCode::NOT_FOUND).set_detail("Oops! Page not found")
    }
    else if rejection.is_not_found() {
        HttpApiProblem::with_title_from_status(StatusCode::NOT_FOUND).set_detail("Oops! Page not found")
    }
    else {
        HttpApiProblem::with_title_from_status(StatusCode::INTERNAL_SERVER_ERROR)
    };

    let status = match problem.status {
        Some(status) => status,
        None => StatusCode::INTERNAL_SERVER_ERROR,
    };

    let reply = Ok(problem).json_reply().with_status(status).err_reply(CookieJar::new());
    if let Some(reply) = reply.ok() {
        Ok(Box::new(reply) as Box<dyn Reply>)
    }
    else {
        Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR) as Box<dyn Reply>)
    }
}