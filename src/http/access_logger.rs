#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use std::fmt::{Display, Formatter, Result};
use warp::filters::log::Info;

pub fn logger(info: Info) {
    info!(
        target: "access_logger",
        "🚀 {remote} \"{method} {path} {version:?}\" {status} \"{referer}\" \"{user_agent}\" {elapsed:?}", 
        remote = OptFmt(info.remote_addr()),
        method = info.method(),
        path = info.path(),
        version = info.version(),
        status = info.status(),
        referer = OptFmt(info.referer()),
        user_agent = OptFmt(info.user_agent()),
        elapsed = info.elapsed(),
    )
}

struct OptFmt<T>(Option<T>);

impl<T: Display> Display for OptFmt<T> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        if let Some(ref t) = self.0 {
            Display::fmt(t, f)
        } else {
            f.write_str("-")
        }
    }
}
