#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use warp::Filter;
use cookie::CookieJar;

pub fn jar() -> impl Filter<Extract = (CookieJar,), Error = warp::Rejection> + Clone {
    use warp::{any, path::full, filters::header, http::header::COOKIE};

    any()
        .and(full())
        .and(header::optional(COOKIE.as_str()))
        .map(|_, cookie: Option<String>| {
            debug!("🍪 {} from request {:?}", COOKIE.as_str(), cookie);
            if let Some(cookie) = cookie {
                cookie.split("; ").map(|val| val.parse().ok()).collect()
            }
            else { vec![] }
        })
        .map(|cookies| {
            let mut jar = CookieJar::new();
            for cookie in cookies {
                if let Some(cookie) = cookie {
                    jar.add_original(cookie);
                }
            }
            jar
        })
}