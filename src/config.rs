use serde::Deserialize;
use std::net::SocketAddr;

#[derive(Debug, Deserialize)]
pub struct AppConfig {
    #[serde(default = "default_host")]
    host: String,
    #[serde(default = "default_port")]
    port: u16,
    #[serde(default = "default_database_url")]
    database_url: String,
    #[serde(default = "default_log_config")]
    log_config: String,
}

impl AppConfig {
    pub fn addr(&self) -> SocketAddr {
        format!("{}:{}", self.host, self.port).parse().expect("Socket address expected")
    }

    pub fn database_url(&self) -> String {
        self.database_url.to_owned()
    }

    pub fn log_config(&self) -> String {
        self.log_config.to_owned()
    }
}

fn default_host() -> String {
    "127.0.0.1".to_string()
}

fn default_port() -> u16 {
    8000
}

fn default_database_url() -> String {
    crate::db::DEFAULT_DATABASE_URL.to_string()
}

fn default_log_config() -> String {
    "log4rs.yaml".to_string()
}