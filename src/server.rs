#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use std::convert::Infallible;
use std::net::SocketAddr;
use warp::{Filter, Reply};

pub async fn start(addr: SocketAddr) {
    warp::serve(routes()).run(addr).await;
}

fn routes() -> impl Filter<Extract = (impl Reply,), Error = Infallible> + Clone {
    warp::any()
        .and(crate::api::module_routes())
        .recover(crate::http::errors::unpack_problem)
        .with(warp::log::custom(crate::http::access_logger::logger))
}