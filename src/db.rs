use lazy_static::*;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use diesel::r2d2::ConnectionManager;

pub const DATABASE_URL_KEY: &str = "database_url";
pub const DEFAULT_DATABASE_URL: &str = "file:point.db";

pub type DBConnection = PgConnection;
type Pool = r2d2::Pool<ConnectionManager<DBConnection>>;

lazy_static! {
    pub static ref POOL: Pool = {
        dotenv().ok();

        let database_url = env::var(DATABASE_URL_KEY).expect(&format!("{} must be set!", DATABASE_URL_KEY));

        let manager = ConnectionManager::<DBConnection>::new(database_url);
        let pool: Pool = r2d2::Pool::builder()
            .max_size(5)
            .build(manager)
            .expect("Failed to create pool.");
        pool
    };
}