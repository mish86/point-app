#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::fmt::{self, Formatter, Display};
use serde::{Serialize, Deserialize};
use diesel::prelude::*;
use super::{execute, execute_create2 as execute_create, FetchResult};
use anyhow::anyhow;

use crate::schema::users;

#[derive(Debug, Clone, Serialize, Deserialize, Identifiable, Queryable)]
#[table_name="users"]
// Confidential information
pub struct User {
    pub id: i32,
    pub username: String,
    pub realname: String,
    pub password: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
// Public information
pub struct OpenUser {
    pub id: i32,
    pub username: String,
}

impl Display for User {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.username)?;
        Ok(())
    }
}

impl PartialEq for User {
    fn eq(&self, other: &User) -> bool {
        self.id == other.id
    }
}

#[derive(Debug, Serialize, Deserialize, Insertable, PartialEq)]
#[table_name="users"]
pub struct NewUser {
    pub username: String,
    pub realname: String,
    pub password: String,
}

impl Display for NewUser {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.username)?;
        Ok(())
    }
}

impl NewUser {
    pub fn validate(self) -> anyhow::Result<Self> {
        let NewUser { ref username, ref realname, ref password } = self;
        // TODO: think about nicer solution
        if username.is_empty() || realname.is_empty() || password.is_empty() {
            Err(anyhow!("Empty values are not allowed!"))
        }
        else {
            Ok(self)
        }
    }

    pub fn hash_password(self) -> anyhow::Result<Self> {
        let password = bcrypt::hash(&self.password, bcrypt::DEFAULT_COST)?;
        Ok(NewUser {
            password,
            ..self
        })
    }
}

impl User {
    pub fn public(&self) ->OpenUser {
        OpenUser { 
            id: self.id.to_owned(), 
            username: self.username.to_owned(),
        }
    }

    pub fn get(uid: i32) -> FetchResult<User> {
        use crate::schema::users::dsl as u;

        execute(|connection| 
            u::users
                .find(uid)
                .first(connection)
        )
    }

    pub fn find(name: &str) -> FetchResult<User> {
        execute(|connection| {
            use crate::schema::users::dsl::{username, users};
            users.filter(username.eq(name)).first(connection)
        })
    }

    pub fn create(new_user: &NewUser) -> FetchResult<User> {
        execute_create(&new_user, 
            |connection| {
            use crate::schema::users::dsl::{id, username, realname, password};            
            diesel::insert_into(users::table)
                .values(new_user)
                .returning((id, username, realname, password))
                .get_result::<User>(connection)
        })
    }
}
