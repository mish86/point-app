#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use derivative::*;
use serde::{Serialize, Deserialize};
use diesel::prelude::*;
use super::{execute, execute_delete, execute_create2 as execute_create, execute_update, FetchResult};
use multipart_reader::*;

use crate::schema::services;

#[derive(Debug, Default, Serialize, Deserialize, MultipartReader, Identifiable, Queryable, AsChangeset)]
#[table_name="services"]
#[changeset_options(treat_none_as_null="true")]
pub struct Service {
    pub id: i32,
    pub summary: String,
    pub amount: f32,
    pub description: Option<String>,
}

#[derive(Derivative, Default, Serialize, Deserialize, MultipartReader, Identifiable, Queryable, AsChangeset)]
#[derivative(Debug)]
#[table_name="services"]
#[changeset_options(treat_none_as_null="true")]
pub struct ServiceFull {
    pub id: i32,
    pub summary: String,
    pub amount: f32,
    pub description: Option<String>,
    #[multipart(callback="read_image")]
    #[derivative(Debug="ignore")]
    pub cover: Option<Vec<u8>>,
}

#[derive(Derivative, Default, Serialize, Deserialize, MultipartReader, Insertable)]
#[derivative(Debug)]
#[table_name="services"]
pub struct NewService {
    pub summary: String,
    pub amount: f32,
    pub description: Option<String>,
    #[multipart(callback="read_image")]
    #[derivative(Debug="ignore")]
    pub cover: Option<Vec<u8>>,
}

impl Service {
    pub fn list() -> FetchResult<Vec<Service>> {
        use crate::schema::services::dsl::*;

        debug!("Loading list of Short Services");
        execute(|connection| 
            services
            .select((
                id, 
                summary,
                amount,
                description.nullable(),
            ))
            .load(connection)
        )
    }

    pub fn get(uid: i32) -> FetchResult<Service> {
        use crate::schema::services::dsl::*;

        debug!("Loading a Short Service by {}", uid);
        execute(|connection| 
            services
                .select((
                    id, 
                    summary,
                    amount,
                    description.nullable(),
                ))
                .find(uid)
                .first(connection)
        )
    }

    pub fn get_full(uid: i32) -> FetchResult<ServiceFull> {
        use crate::schema::services::dsl::*;

        debug!("Loading a Full Service by {}", uid);
        execute(|connection| 
            services
                .find(uid)
                .first(connection)
        )
    }

    pub fn create(new_service: &NewService) -> FetchResult<Service> {
        debug!("Creating a New Service by {:#?}", new_service);

        execute_create(&new_service, 
            |connection| {
            use crate::schema::services::dsl::{id, summary, amount, description};
            diesel::insert_into(services::table)
                .values(new_service)
                .returning((id, summary, amount, description))
                .get_result::<Service>(connection)
        })
    }

    pub fn update_full(service: &ServiceFull) -> FetchResult<()> {
        debug!("Updating {:?}", service);
        
        execute_update( service.id, |connection| {
            use crate::schema::services::dsl::*;
            diesel::update(services.filter(id.eq(service.id)))
                .set(service)
                .execute(connection)
        })
    }

    pub fn update(service: &Service) -> FetchResult<()> {
        debug!("Updating {:?}", service);
        
        execute_update( service.id, |connection| {
            use crate::schema::services::dsl::*;
            diesel::update(services.filter(id.eq(service.id)))
                .set(service)
                .execute(connection)
        })
    }

    pub fn update_cover(service: &ServiceFull) -> FetchResult<()> {
        debug!("Updating cover for {:?}", service);
        
        execute_update( service.id, |connection| {
            use crate::schema::services::dsl::*;
            diesel::update(services.filter(id.eq(service.id)))
                .set((
                    cover.eq(&service.cover),
                ))
                .execute(connection)
        })
    }

    pub fn delete(uid: i32) -> FetchResult<()> {
        debug!("Deleting a Service by {}", uid);

        execute_delete(uid, |connection| {
            use crate::schema::services::dsl::{services, id};
            diesel::delete(services.filter(id.eq(uid))).execute(connection)
        })
    }
}

fn read_image(data: impl bytes::Buf) -> anyhow::Result<Option<Vec<u8>>> {
    let img = image::load_from_memory(data.bytes())?;
    let img = img.resize(900, 900, image::imageops::FilterType::Lanczos3);
    let mut thumbnail = Vec::new();
    img.write_to(&mut thumbnail, image::ImageFormat::Jpeg)?;
    Ok(Some(thumbnail))
}

impl From<ServiceFull> for Service {
    fn from(service: ServiceFull) -> Self {
        Service {
            id: service.id,
            summary: service.summary,
            description: service.description,
            amount: service.amount,
        }
    }
}