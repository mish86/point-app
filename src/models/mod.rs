//! Models
#[allow(unused_imports)] 
use log::{error, warn, info, debug, trace};
use anyhow;
use diesel::prelude::*;

use crate::{db, db::DBConnection};

pub mod user;
pub mod service;

/// DB fetch error
#[derive(Debug, thiserror::Error)]
pub enum FetchError {
    /// r2d2 connection pool error
    #[error("{0}")]
    ConnectionError(#[from] r2d2::Error),
    /// diesel orm query error
    #[error("{0}")]
    QueryError(#[from] diesel::result::Error),
}

impl From<FetchError> for String {
    fn from(item: FetchError) -> Self {
        format!("{}", item)
    }
}

pub type FetchResult<T> = anyhow::Result<T>;

pub fn execute<R, F>(fnc: F) -> FetchResult<R>
where F: FnOnce(&DBConnection) -> QueryResult<R>
{
    let type_name = std::any::type_name::<R>();
    let fnc_name = std::any::type_name::<F>();
    trace!("Executing {type_name} -> {fnc_name}", type_name = type_name, fnc_name = fnc_name);

    match db::POOL.get() {
        Ok(connection) => {
            fnc(&connection)
                .map_err(|error| {
                    match error {
                        diesel::result::Error::NotFound => warn!("No data found for {fnc_name}", fnc_name = fnc_name),
                        _ => error!("Failed to execute {fnc_name} due to {error:?}", fnc_name = fnc_name, error = error),
                    }
                    // FetchError::QueryError(error)
                    anyhow::Error::new(FetchError::QueryError(error))
                })
        },
        Err(error) => {
            error!("Failed to obrtain connection for {fnc_name} {error:?}", fnc_name = fnc_name, error = error);
            Err(anyhow::Error::new(FetchError::ConnectionError(error)))
        }
    }
}

pub type DeleteResult = QueryResult<usize>;

pub fn execute_delete<UID, F>(uid: UID, fnc: F) -> FetchResult<()>
where 
    F: FnOnce(&DBConnection) -> DeleteResult,
    UID: std::fmt::Display,
{
    info!("Deleting entity by {}", uid);

    execute(|connection| {
        match fnc(&connection) {
            Ok(row) => {
                if row == 0 {
                    error!("Failed to delete entity with id {} in model unit due to not found", uid);
                    Err(diesel::result::Error::NotFound)
                } else {
                    Ok(())
                }
            },
            Err(error) => {
                error!("Failed to delete entity with id {} in model unit due to\n{:#?}", uid, error);
                Err(error)
            },
        }
    })
}

pub fn execute_update<UID, F>(uid: UID, fnc: F) -> FetchResult<()>
where 
    F: FnOnce(&DBConnection) -> DeleteResult,
    UID: std::fmt::Display,
{
    info!("Updating entity by {}", uid);

    execute(|connection| {
        match fnc(&connection) {
            Ok(row) => {
                if row == 0 {
                    error!("Failed to update entity with id {} in model unit due to not found", uid);
                    Err(diesel::result::Error::NotFound)
                } else {
                    Ok(())
                }
            },
            Err(error) => {
                error!("Failed to update entity with id {} in model unit due to\n{:#?}", uid, error);
                Err(error)
            },
        }
    })
}

#[allow(dead_code)]
pub type CreateResult = QueryResult<usize>;

#[allow(dead_code)]
pub fn execute_create<NT, T, F, R>(new_entity: NT, create_fnc: F, returning_fnc: R) -> FetchResult<T>
where 
    F: FnOnce(&DBConnection) -> CreateResult,
    R: FnOnce(&DBConnection) -> Result<T, diesel::result::Error>,
    NT: std::fmt::Debug,
{
    info!("Creating new entity {:?}", new_entity);

    execute(|connection| {
        match create_fnc(connection) {
            Ok(rows) => {
                if rows == 0 {
                    error!("Failed to insert entity {:?}", new_entity);
                    Err(diesel::result::Error::NotFound)
                } else {
                    returning_fnc(connection)
                }
            },
            Err(error) => {
                error!("Failed to insert entity {:?}", new_entity);
                Err(error)
            },
        }
    })
}

pub type CreateResult2<T> = QueryResult<T>;

pub fn execute_create2<NT, T, F>(new_entity: NT, create_fnc: F) -> FetchResult<T>
where 
    F: FnOnce(&DBConnection) -> CreateResult2<T>,
    NT: std::fmt::Debug,
{
    info!("Creating new entity {:?}", new_entity);

    execute(|connection| {
        match create_fnc(connection) {
            Ok(res) => {
                Ok(res)
            },
            Err(error) => {
                error!("Failed to insert entity {:?}", new_entity);
                Err(error)
            },
        }
    })
}