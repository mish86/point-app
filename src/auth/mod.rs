use lazy_static::*;
use time::Duration;
use cookie::Cookie;

pub mod session_factory;
pub mod session;
pub mod user;
pub mod authorization;

#[allow(dead_code)]
pub const SID_COOKIE_KEY: &str = session::SID_COOKIE_KEY;
#[allow(dead_code)]
pub const USER_COOKIE_KEY: &str = user::USER_COOKIE_KEY;

lazy_static! {
    pub static ref SESSION_MAX_AGE: Duration = Duration::minutes(5);
    static ref DEFAULT_MAX_AGE: Duration = Duration::minutes(5);
}


pub type User = crate::models::user::User;
pub type NewUser = crate::models::user::NewUser;
pub type OpenUser = crate::models::user::OpenUser;
pub type LoginForm = user::LoginForm;
pub type UserError<'r> = user::UserError<'r>;
pub type Session = session::Session;
pub type SessionError = session::SessionError;
pub type SessionFactory = session_factory::SessionFactory;
pub type Authorization = authorization::Authorization;
// pub type OpenAuthorization = authorization::OpenAuthorization;
pub type AuthorizationError<'r> = authorization::AuthorizationError<'r>;

pub trait CookieBuilder {
    fn max_age(&self) -> Duration {
        *DEFAULT_MAX_AGE
    }

    fn cookie_key<'a>() -> &'a str;

    fn build_new_cookie<'a>(&self) -> Cookie<'a>;

    fn build_del_cookie<'a>() -> Cookie<'a> {
        Cookie::build(Self::cookie_key(), "")
            .max_age(Duration::zero())
            .http_only(true)
            .finish()
    }
}