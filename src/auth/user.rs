#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::borrow::Cow;
use serde::{Serialize, Deserialize};
use cookie::{Cookie, SameSite};

use super::CookieBuilder;
use super::User;

pub const USER_COOKIE_KEY: &str = "user_id";

/// Session Error
#[derive(Debug, thiserror::Error)]
pub enum UserError<'r> {
    #[error("Empty user")]
    Empty,
    #[error("Invalid ID: {0}")]
    InvalidId(Cow<'r, str>),
    #[error("{0} not found")]
    NotFound(Cow<'r, str>),
}

#[allow(dead_code)]
impl<'r> UserError<'r> {
    pub fn invalid_id<S>(raw: S) -> UserError<'r>
    where S: Into<Cow<'r, str>> {
        UserError::InvalidId(raw.into())
    }

    pub fn not_found<S>(raw: S) -> UserError<'r>
    where S: Into<Cow<'r, str>> {
        UserError::NotFound(raw.into())
    }

    pub fn into_error(self) -> anyhow::Error
    where Self: 'r + 'static {
        anyhow::Error::msg(self)
    }
}

impl CookieBuilder for User {

    fn cookie_key<'a>() -> &'a str {
        USER_COOKIE_KEY
    }

    /// Prepares Cookie for server response.
    fn build_new_cookie<'a>(&self) -> Cookie<'a> {
        Cookie::build(USER_COOKIE_KEY, self.id.to_string())
            .path("/")
            .max_age(self.max_age())
            .same_site(SameSite::Strict)
            .finish()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LoginForm {
    pub username: String,
    pub password: String,
}

impl LoginForm {
    pub fn validate(&self) -> anyhow::Result<User> {
        use crate::models::FetchError;
        use super::AuthorizationError;

        let user = User::find(&self.username)
            .map_err(|error| {
                match error.downcast_ref::<FetchError>()  {
                    Some(FetchError::QueryError(diesel::result::Error::NotFound)) => 
                        UserError::not_found(self.username.to_string()).into_error(),
                    _ => error,
                }
            })?;
        
        let is_valid = bcrypt::verify(&self.password, &user.password);

        match is_valid {
            Ok(true) => Ok(user),
            Ok(_) | Err(bcrypt::BcryptError::InvalidPassword) => 
                Err(anyhow::Error::new(AuthorizationError::InvalidCredentials)),
            Err(_) => Err(anyhow::Error::msg(AuthorizationError::MaliciousAccess)),
        }
    }
}