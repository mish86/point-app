#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use lazy_static::*;
use std::collections::HashMap;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};

use super::session::{SessionError, Session};

/// Hashmap to store sessions by id.
type SessionStore = HashMap<String, Session>;

/// Session factory.
pub struct SessionFactory(Arc<RwLock<SessionStore>>);

lazy_static! {
    static ref SESSION_FACTORY: Arc<SessionFactory> = Arc::new(SessionFactory::new());
}

#[allow(dead_code)]
impl SessionFactory {

    /// Creates a new session store.
    fn new() -> Self {
        SessionFactory ({
            let store = SessionStore::with_capacity(100);
            Arc::new(RwLock::new(store))
        })
    }

    /// Returns a pointer for sync instance.
    pub fn instance() -> Arc<Self> {
        Arc::clone(&*SESSION_FACTORY)
    }

    /// Acquires a store for read.
    fn store(&self) -> anyhow::Result<RwLockReadGuard<'_, SessionStore>> {
        self.0.read()
            .map_err(|error| {
                let error = SessionError::AcquireReadError(error.to_string());
                error!("{}", error);
                anyhow::Error::msg(error)
            })
            .map(|store| {
                trace!("Store is acquired for read");
                store
            })
    }

    /// Acquires a store for write.
    fn store_mut(&self) -> anyhow::Result<RwLockWriteGuard<'_, SessionStore>> {
        self.0.write()
            .map_err(|error| {
                let error = SessionError::AcquireWriteError(error.to_string());
                error!("{}", error);
                anyhow::Error::msg(error)
            })
            .map(|store| {
                trace!("Store is acquired for write");
                store
            })
    }

    /// Returns session from store by given id.
    pub fn find(&self, sid: &str) -> Option<Session> {
        self.store()
            .ok()
            .and_then(|store| store.get(sid).cloned())
            .map(|session| {
                debug!("Found session {}", session);
                session
            })
    }

    /// Saves a given session as a cloned to store and return origin one back.
    pub fn save<'s>(&self, session: &'s Session) -> Option<&'s Session> {
        self.store_mut()
            .ok()
            .and_then(|mut store| {
                store.insert(session.id(), session.clone());
                debug!("Session {} is stored", session);
                Some(session)
            })
    }

    /// Removes session by a given id from store and return it back.
    pub fn remove(&self, sid: &str) -> Option<Session> {
        self.store_mut()
            .ok()
            .and_then(|mut store| {
                store.remove(sid)
                    .map_or_else(|| {
                        debug!("No session {} to drop", sid);
                        None
                    }, |session| {
                        debug!("Session {} is dropped", sid);
                        Some(session)
                    })
            })
    }

    /// Removes all sessions in store.
    pub fn clear(&self) -> anyhow::Result<()> {
        self.store_mut()
            .and_then(|mut store| {
                store.clear();
                debug!("All sessions are cleared out!");
                Ok(())
            })
    }

    pub fn remove_inactive(&self) -> anyhow::Result<()> {
        self.store_mut()
            .and_then(|mut store| {
                store.retain(|_, val| {
                    let expired = val.is_expired();
                    if expired {
                        debug!("Session {} is expired and marked for remove", val);
                    }
                    // indicate to keep
                    !expired
                });
                Ok(())
            })
    }
}

