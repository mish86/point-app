#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::fmt::{Display, Debug, Formatter, self};
use time::OffsetDateTime;
use nanoid::nanoid;
use serde::{Serialize, Deserialize};
use cookie::{Cookie, SameSite};

use crate::state::State;
use super::CookieBuilder;

const SID_SIZE: usize = 32;
pub const SID_COOKIE_KEY: &str = "sid";

/// Session Error
#[derive(Debug, thiserror::Error)]
pub enum SessionError {
    #[error("Empty session")]
    Empty,
    #[error("Invalid session {0}")]
    Invalid(String),
    #[error("Session acquiring for read error {0}")]
    AcquireReadError(String),
    #[error("Session acquiring for write error {0}")]
    AcquireWriteError(String),
}

/// Session with server side state.
#[derive(Clone, Debug, Serialize, Deserialize)]
// Confidential information
pub struct Session {
    id: String,
    active_when: OffsetDateTime,
    state: State,
}

// Public information
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OpenSession {
    pub id: String,
}

impl Display for Session {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&self.id, f)
    }
}

impl CookieBuilder for Session {

    fn cookie_key<'a>() -> &'a str {
        SID_COOKIE_KEY
    }

    /// Prepares Cookie for server response.
    fn build_new_cookie<'a>(&self) -> Cookie<'a> {
        Cookie::build(SID_COOKIE_KEY, self.id())
            .path("/")
            .max_age(self.max_age())
            .same_site(SameSite::Strict)
            .finish()
    }
}

#[allow(dead_code)]
impl Session {
    /// Creates a session with generated id.
    pub fn new() -> Self {
        Session {
            id: Self::generate_id(),
            active_when: OffsetDateTime::now(),
            state: State::default(),
        }
    }

    pub fn public(&self) -> OpenSession {
        OpenSession { 
            id: self.id.to_owned(),
        }
    }

    /// Validates given id using [`Session::validate()`] and creates a session.
    pub fn from_sid(sid: &str) -> anyhow::Result<Self> {
        Self::validate(sid.to_string())
            .map(|sid| {
                Session {
                    id: sid,
                    active_when: OffsetDateTime::now(),
                    state: State::default(),
                }
            })
    }

    /// Generates a new id for session.
    pub fn generate_id() -> String {
        nanoid!(SID_SIZE)
    }

    /// Validates a give session id by:
    /// - string length
    pub fn validate(sid: String) -> anyhow::Result<String> {
        match sid.len() == SID_SIZE {
            true => Ok(sid),
            _ => {
                let error = SessionError::Invalid(sid);
                error!("{}", error);
                Err(anyhow::Error::msg(error))
            },
        }
    }

    /// Returns a cloned id of a session.
    pub fn id(&self) -> String {
        self.id.clone()
    }

    /// Last time when session was active.
    pub fn active_when(&self) -> &OffsetDateTime {
        &self.active_when
    }

    /// Extends session by last time activity.
    pub fn retain(&mut self) {
        debug!("Session {} is extended on {} sec", self, self.max_age().whole_seconds());
        self.active_when = OffsetDateTime::now();
    }

    /// Returns if session is expired comparing with last activity time.
    pub fn is_expired(&self) -> bool {
        self.active_when + self.max_age() <= OffsetDateTime::now()
    }

    /// Returns a ref to state of a session. 
    pub fn state(&self) -> &State {
        &self.state
    }

    /// Returns a mutable ref to state of a session. 
    pub fn state_mut(&mut self) -> &mut State {
        &mut self.state
    }
}