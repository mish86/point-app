#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::fmt::{Display, Debug, Formatter, self};
use std::borrow::Cow;
use serde::{Serialize, Deserialize};
use cookie::CookieJar;

use crate::models::user::{User, OpenUser};
use super::session::{Session, OpenSession};
use super::user::LoginForm;
use super::SessionFactory;

/// Authorization Error
#[allow(dead_code)]
#[derive(Debug, thiserror::Error)]
pub enum AuthorizationError<'r> {
    #[error("{0}")]
    Unauthorized(Cow<'r, str>),
    #[error("Malicious Access")]
    MaliciousAccess,
    #[error("Invalid credentials")]
    InvalidCredentials,
    #[error("Invalid Basic Authorization: {0}")]
    InvalidBasicAutorization(Cow<'r, str>),
    #[error("Unsuported authorization type")]
    UnsupportedAuthorization,
}

#[allow(dead_code)]
impl<'r> AuthorizationError<'r> {
    pub fn unauthorized<S>(raw: S) -> AuthorizationError<'r>
    where S: Into<Cow<'r, str>> {
        AuthorizationError::Unauthorized(raw.into())
    }

    pub fn invalid<S>(raw: S) -> AuthorizationError<'r>
    where S: Into<Cow<'r, str>> {
        AuthorizationError::InvalidBasicAutorization(raw.into())
    }

    pub fn into_error(self) -> anyhow::Error
    where Self: 'r + 'static {
        anyhow::anyhow!(self)
    }
}

/// Authorization. Requires a session and authenticated user.
/// Used to restrict permissions on routes.
// Confidential information
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Authorization {
    pub user: User,
    pub session: Session,
}


// Public information
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OpenAuthorization {
    pub user: OpenUser,
    pub session: OpenSession,
}

impl Display for Authorization {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{} authorized in {}`", self.user, self.session)?;
        Ok(())
    }
}

#[allow(dead_code)]
impl Authorization {

    /// Creates a new authorization using given user and session.
    pub fn new(user: User, session: Session) -> Self {
        Authorization {user, session}
    }

    pub fn public(&self) -> OpenAuthorization {
        OpenAuthorization { 
            user: self.user.public(),
            session: self.session.public(),
        }
    }

    /// Executes authentication of user from login form and given session.
    pub fn authenticate(login: &LoginForm, session: &Session) -> anyhow::Result<Self> {
        login.validate()
            .map(|user| Authorization::new(user, session.clone()))
    }

    /// Validates the authenticated user is present in web session.
    pub fn validate(&self) -> anyhow::Result<()> {
        let saved_user_id = self.session.state().get(super::USER_COOKIE_KEY);
        saved_user_id
            .and_then(|saved_user_id: i32| {
                if self.user.id == saved_user_id {
                    Some(())
                }
                else { None }
            })
            .ok_or_else(|| AuthorizationError::MaliciousAccess.into_error())
            .map_err(|err| {
                error!("🚫 User {} triggers {} in {}", self.user.id, err, self.session.id());
                err
            })
    } 

    pub fn save(&mut self, jar: &mut CookieJar) {
        use super::CookieBuilder;

        let Authorization {ref user, ref mut session} = self;
            
        session.retain();
        session.state_mut().insert(super::USER_COOKIE_KEY, user.id).ok();
        SessionFactory::instance().save(&session);

        jar.add(user.build_new_cookie());
        jar.add(session.build_new_cookie());
    }

    pub fn clear(jar: &mut CookieJar) {
        use super::CookieBuilder;

        jar.add(User::build_del_cookie());
        jar.add(Session::build_del_cookie());
    }

    pub fn clear_all(&mut self, jar: &mut CookieJar) {
        SessionFactory::instance().remove(&self.session.id());
        Self::clear(jar);
    }
}