/// Helper to combine the multiple filters together with Filter::or, possibly boxing the types in
/// the process.
/// https://github.com/seanmonstar/warp/issues/507#issuecomment-604536920
/// https://github.com/ipfs-rust/rust-ipfs/commit/ae3306686209afa5911b1ad02170c1ac3bacda7c#diff-42186beaaed2582f7007203c90025abb
#[macro_export]
macro_rules! combine {
    ($x:expr, $($y:expr),+) => {
        {
            let filter = boxed_on_debug!($x);
            $(
                let filter = boxed_on_debug!(filter.or($y));
            )+
            filter
        }
    };
    ($x:expr, $($y:expr,)+) => {
        {
            combine!($x, $($y),+)
        }
    };
}

#[macro_export]
#[cfg(debug_assertions)]
macro_rules! boxed_on_debug {
    ($x:expr) => {
        $x.boxed()
    };
}

#[macro_export]
#[cfg(not(debug_assertions))]
macro_rules! boxed_on_debug {
    ($x:expr) => {
        $x
    };
}