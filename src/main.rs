#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate cookie;

#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use log4rs;
use std::env;
use std::fs;
use toml;

mod lib;
mod config;
mod db;
mod schema;
mod models;
mod state;
mod auth;
mod http;
mod api;
mod server;

use config::AppConfig;

const APP_CONFIG_FILE: &str = "config.toml";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    
    // change current working directory to app directory
    change_cwd()?;
    
    let config_path = {
        let mut config_path = env::current_dir()?;
        config_path.push(APP_CONFIG_FILE);
        config_path
    };
    let config = fs::read_to_string(&config_path)
        .expect(&format!("Cannot read {:?}", config_path));
    let config: AppConfig = toml::from_str(&config)?;

    log4rs::init_file(config.log_config(), Default::default())?;
    
    info!("🌍 Booting up");
    info!("🛠 Server config {:#?}", config);
    
    env::set_var(db::DATABASE_URL_KEY, config.database_url());
    let addr = config.addr();

    // cleansing of inactive sessions
    tokio::spawn(async {
        let duration: time::Duration = *auth::SESSION_MAX_AGE;
        let duration = tokio::time::Duration::from_secs(duration.whole_seconds() as u64);
        loop {
            tokio::time::delay_for(duration).await;
            info!("🧹 Cleansing of inactive sessions");
            auth::SessionFactory::instance().remove_inactive().ok();
        }
    });

    // start web server
    server::start(addr).await;

    Ok(())
}

#[cfg(debug_assertions)]
fn change_cwd() -> std::io::Result<()> {
    // Do Nothing
    Ok(())
}

#[cfg(not(debug_assertions))]
fn change_cwd() -> std::io::Result<()> {
    let mut config_path = env::current_exe()?;
    config_path.pop();
    env::set_current_dir(&config_path)?;
    Ok(())
}
