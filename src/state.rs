//! Views

#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::fmt::Debug;
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use serde_json::Value;
use anyhow;

pub type Store = HashMap<String, Value>;
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct State(Store);

#[allow(dead_code)]
impl State {
    pub fn new() -> Self {
        State(HashMap::new())
    }

    pub fn new_from<T: Serialize + Debug>(key: &str, val: T) -> anyhow::Result<Self> {
        let mut state = State::new();
        state.insert(key, val)?;
        Ok(state)
    }

    pub fn insert<T: Serialize + Debug>(&mut self, key: &str, val: T) -> anyhow::Result<()> {
        self.0.insert(key.to_string(), to_value(&val)?);
        Ok(())
    }

    pub fn get<T>(&self, key: &str) -> Option<T>
    where for<'de> T: Deserialize<'de>
    {
        let val = self.0.get(key).cloned()?;
        serde_json::value::from_value(val)
            .map_err(|error| {
                error!("Failed to convert value {:#?} due {}", self.0.get(key), error);
            })
            .ok()
    }

    pub fn get_value(&self, key: &str) -> Option<Value> {
        self.0.get(key).cloned()
    }

    pub fn remove<T>(&mut self, key: &str) -> Option<T>
    where for<'de> T: Deserialize<'de>
    {
        let removed = self.0.remove(key)?;
        serde_json::value::from_value(removed).ok()
    }

    pub fn clear(&mut self) {
        self.0.clear();
    }
}

fn to_value<T>(val: &T) -> anyhow::Result<Value>
where
    T: Serialize + Debug,
{
    serde_json::value::to_value(&val).map_err(|err| {
        error!("Failed to convert {:#?} into JSON due to {}", val, err);
        anyhow::Error::msg(err)
    })
}