table! {
    services (id) {
        id -> Int4,
        summary -> Varchar,
        amount -> Float4,
        description -> Nullable<Text>,
        cover -> Nullable<Bytea>,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        realname -> Varchar,
        password -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    services,
    users,
);
