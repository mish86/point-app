pub use multipart_reader_derive::*;
#[allow(dead_code)]
pub type MultipartReaderCallback<T> = fn(obj: T, data: Box<dyn bytes::Buf>) -> anyhow::Result<()>;

pub use async_trait::async_trait;

#[derive(Debug, thiserror::Error)]
pub enum MultipartError {
    #[error("Object {0} does not have field: {1}")]
    NonExistingField(String, String),
}

#[allow(dead_code)]
impl MultipartError {
    pub fn non_existing_field<S>(obj: S, field: S) -> MultipartError
    where S: Into<String> {
        MultipartError::NonExistingField(obj.into(), field.into())
    }

    pub fn into_error(self) -> anyhow::Error {
        anyhow::Error::msg(self)
    }
}

#[async_trait]
pub trait MultipartReader {
    async fn read(&mut self, mut data: warp::multipart::FormData) -> anyhow::Result<()> {
        use tokio::stream::*;
        
        while let Some(part) = data.next().await {
            let mut part = part.map_err(anyhow::Error::from)?;
    
            let data = part.data().await
                .ok_or_else(|| anyhow::Error::msg("Empty Data"))?;
            let data = data.map_err(anyhow::Error::from)?;
            
            let data = Box::new(data);
            self.read_field(part.name(), data)?;
        }
        Ok(())
    }

    fn read_field(&mut self, field: &str, data: Box<dyn bytes::Buf>) -> anyhow::Result<()>;
}

pub struct Data(pub Box<dyn bytes::Buf>);

impl From<Data> for anyhow::Result<String> {
    fn from(val: Data) -> Self {
        read_string(val.0)
    }
}

impl From<Data> for anyhow::Result<f32> {
    fn from(val: Data) -> Self {
        read_f32(val.0)
    }
}

impl From<Data> for anyhow::Result<i32> {
    fn from(val: Data) -> Self {
        read_i32(val.0)
    }
}

impl From<Data> for anyhow::Result<Vec<u8>> {
    fn from(val: Data) -> Self {
        read_bytes(val.0)
    }
}

impl<T> From<Data> for anyhow::Result<Option<T>>
where anyhow::Result<T>: From<Data> {
    fn from(val: Data) -> Self {
        let val: anyhow::Result<T> = val.into();
        Ok(Some(val?))
    }
}

fn read_string(data: impl bytes::Buf) -> anyhow::Result<String> {
    std::str::from_utf8(data.bytes())
        .map(std::string::ToString::to_string)
        .map_err(anyhow::Error::from)
}

fn read_i32(data: impl bytes::Buf) -> anyhow::Result<i32> {
    let val = read_string(data)?;
    val.parse::<i32>().map_err(anyhow::Error::from)
}

fn read_f32(data: impl bytes::Buf) -> anyhow::Result<f32> {
    let val = read_string(data)?;
    val.parse::<f32>().map_err(anyhow::Error::from)
}

fn read_bytes(data: impl bytes::Buf) -> anyhow::Result<Vec<u8>> {
    Ok(data.bytes().into())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[allow(unused_variables)]
    fn test1() {
        #[derive(MultipartReader)]
        struct SimplePojo {
            #[multipart(skip)]
            #[allow(dead_code)]
            skipped: bool,
            #[multipart(callback="read_bool")]
            custom_callback: bool,
            id: i32,
            name: String,
            amount1: f32,
            amount2: Option<f32>,
            image: Option<Vec<u8>>,
        }

        fn read_bool(data: impl bytes::Buf) -> anyhow::Result<bool> {
            let val = read_string(data)?;
            val.parse::<bool>().map_err(anyhow::Error::from)
        }

        // #[derive(MultipartReader)]
        // struct Unnamed (String);

        let mut pojo = SimplePojo {
            skipped: false,
            custom_callback: false,
            id: 123,
            name: "Simple Pojo".to_string(),
            amount1: 0f32,
            amount2: Some(0f32),
            image: Some(b"blob"[..].to_vec()),
        };
        assert_eq!(pojo.skipped, false);
        assert_eq!(pojo.custom_callback, false);
        assert_eq!(pojo.name.as_str(), "Simple Pojo");
        assert_eq!(pojo.amount1, 0f32);        
        assert_ne!(pojo.amount2, None);
        assert_eq!(pojo.image, Some("blob".to_string().into_bytes()));

        let new_name = &b"New Simple Pojo"[..];
        let name_field_is_set = pojo.read_field("name", Box::new(new_name));
        assert_eq!(name_field_is_set.is_ok(), true, "name field is set");
        assert_eq!(pojo.name.as_str(), "New Simple Pojo");

        let new_amount = &b"555.55"[..];
        let amount1_field_is_set = pojo.read_field("amount1", Box::new(new_amount));
        assert_eq!(amount1_field_is_set.is_ok(), true, "amount1 field is set");
        assert_eq!(pojo.amount1, 555.55f32);
        let amount2_field_is_set = pojo.read_field("amount2", Box::new(new_amount));
        assert_eq!(amount2_field_is_set.is_ok(), true, "amount2 field is set");
        assert_eq!(pojo.amount2, Some(555.55f32));

        let new_blob= &b"PNG"[..];
        let image_field_is_set = pojo.read_field("image", Box::new(new_blob));
        assert_eq!(image_field_is_set.is_ok(), true, "image field is set");
        assert_eq!(pojo.image, Some("PNG".to_string().into_bytes()));

        let new_bool= &b"true"[..];
        let custom_callback_is_set = pojo.read_field("custom_callback", Box::new(new_bool));
        assert_eq!(custom_callback_is_set.is_ok(), true, "custom_callback_is_set field is set");
        assert_eq!(pojo.custom_callback, true);

        let new_name= &b"non-existing field"[..];
        let non_existing_field_is_not_set = pojo.read_field("non-existing field", Box::new(new_name));
        assert_eq!(non_existing_field_is_not_set.is_err(), true, "non-existing field is NOT set");
    }
}
