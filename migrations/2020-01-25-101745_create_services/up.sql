CREATE TABLE services (
    id SERIAL PRIMARY KEY,
    -- id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    summary VARCHAR NOT NULL,
    amount REAL NOT NULL,
    description TEXT,
    cover bytea 
);

CREATE TABLE users (
    -- id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    id SERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    realname VARCHAR NOT NULL,
    password VARCHAR NOT NULL
);

CREATE UNIQUE INDEX users_username_idx ON users(username);

INSERT INTO users (username, realname, password)
values ('admin', 'admin', '$2y$10$UwjPraKUflt4vm0OJurbD.NP3wjdY.t0i1bo.GyfR9oTUbv5xPzei');