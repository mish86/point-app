# Point Web

## Prerequisites sqlite
Please refer to [Diesel](http://diesel.rs/guides/getting-started/)
1. (Windows only) Rollout [sqlite](https://www.sqlite.org/download.html),https://github.com/diesel-rs/diesel/issues/487
    - Download and unzip sqlite3.def and sqlite3.dll into <path_to_sqlite_dir>
    - set SQLITE3_LIB_DIR "<path_to_sqlite_dir>"
    - set LIB "<path_to_sqlite_dir>"
    - Run x86_x64 Cross Tools Command Prompt for VS 2019
    - cd <path_to_sqlite_dir>
    - lib /def:sqlite3.def /machine:X64 /out:sqlite3.lib
2. cargo install diesel_cli --no-default-features --features sqlite
3. echo "DATABASE_URL=file:test.db" > .env
4. diesel migration run --database-url point.db

## Prerequisites postgres
Install and start (stop) postgres
1. (macos only) brew install postgresql
2. pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log -p /usr/local/bin/postgres start
3. createuser --superuser --pwprompt postgres
4. prompt password "postgres"
5. (Optional) pg_ctl -D /usr/local/var/postgres stop

Please refer to [Diesel](http://diesel.rs/guides/getting-started/)
1. cargo install diesel_cli --no-default-features --features postgres
2. echo DATABASE_URL=postgres://postgres:postgres@localhost/point_db > .env
3. diesel setup
4. diesel migration run

## TODO

## Road Map
- [x] Rest CRUD services
- [x] Logging
- [x] Backend auth restrictions/valications
- [x] Rest CRUD users
- [x] React
    - [x] Login
    - [x] CRUD services forms
    - [x] Sign up
- [x] Split Client and Backend
    - [ ] Client < nginx configuration
    - [ ] Backend > nginx configuration
- [ ] Pipeline configuration
    - [ ] Build
    - [ ] Client docker image
        - [ ] Client docker image with nginx
    - [ ] Backend docket image
        - [ ] sqlite driver installation
        - [ ] run diesle migration on start up
        - [ ] Rust build configutation
        - [ ] Client docker image with nginx
    - [ ] GitLab configuration
- [ ] Rest CRUD appointments
- [ ] Basic service appointment flow
- [ ] Event in calendar for Service Provider
    - [ ] https://github.com/ramosbugs/oauth2-rs/blob/master/examples/google.rs
    - [ ] https://developers.google.com/calendar/create-events
- [ ] Tests
- [ ] Docs

## Links
### Compilation speed up
https://github.com/seanmonstar/warp/issues/507#issuecomment-604536920
https://github.com/ipfs-rust/rust-ipfs/commit/ae3306686209afa5911b1ad02170c1ac3bacda7c#diff-42186beaaed2582f7007203c90025abb