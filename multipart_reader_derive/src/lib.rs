extern crate proc_macro;

mod multipart;

#[proc_macro_derive(MultipartReader, attributes(multipart))]
pub fn derive_multipart_reader(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    multipart::impl_multipart_reader(input)
}