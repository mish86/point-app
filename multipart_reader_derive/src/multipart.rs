use darling::{FromDeriveInput, FromField};
use proc_macro::TokenStream;
use quote::{quote, ToTokens};
use syn::DeriveInput;

#[derive(Clone, Debug, FromField)]
#[darling(attributes(multipart))]
struct ModelField {
    ident: Option<syn::Ident>,
    ty: syn::Type,
    #[darling(default="impl_default_skip")]
    skip: bool,
    #[darling(default)]
    callback: String,
}

fn impl_default_skip() -> bool { false }

#[derive(Debug, FromDeriveInput)]
#[darling(attributes(multipart), supports(struct_named))]
struct Model {
    ident: syn::Ident,
    data: darling::ast::Data<darling::util::Ignored, ModelField>,
}

impl Model {
    fn impl_field_callbacks(&self) -> impl ToTokens {
        let fields = self.data.clone().take_struct().unwrap();
        let callbacks = fields.iter()
            .filter(|field| !field.skip)
            .map(|field| {
                let ident = &field.ident;
                let ident_name = format!("{}", quote!(#ident));
                let ty = &field.ty;
                let custom_callback = &field.callback;
                if custom_callback.is_empty() {
                    quote! {
                        map.insert(#ident_name, |obj, bytes| {
                            let val: anyhow::Result<#ty> = Data(bytes).into();
                            obj.#ident = val?;
                            Ok(())
                        })
                    }
                }
                else {
                    let custom_callback = proc_macro2::Ident::new(custom_callback, proc_macro2::Span::call_site());
                    quote! {
                        map.insert(#ident_name, |obj, bytes| {
                            let val: anyhow::Result<#ty> = #custom_callback(bytes);
                            obj.#ident = val?;
                            Ok(())
                        })
                    }
                }
            });
        let ident = &self.ident;

        quote! {
            let mut map = std::collections::HashMap::<&str, MultipartReaderCallback<&mut #ident>>::new();
            #(
                #callbacks;
            )*
        }
    }
}

impl ToTokens for Model {
    fn to_tokens(&self, out: &mut proc_macro2::TokenStream) {
        let ident = &self.ident;
        let ident_name = format!("{}", quote!(#ident));
        let field_callbacks = self.impl_field_callbacks();

        let tokens = quote! {
            #[async_trait]
            impl MultipartReader for #ident {
                fn read_field(&mut self, field: &str, bytes: Box<dyn bytes::Buf>) -> anyhow::Result<()> {
                    
                    #field_callbacks
                    
                    let cb = map.get(field)
                        .ok_or_else(|| MultipartError::non_existing_field(#ident_name, field).into_error())?;

                    cb(self, bytes)?;
                    Ok(())
                }
            }
        };
        out.extend(tokens)
    }
}


pub fn impl_multipart_reader(input: TokenStream) -> TokenStream {
    let input: DeriveInput = syn::parse(input).unwrap();
    let from_model = match Model::from_derive_input(&input) {
        Ok(parsed) => parsed,
        Err(e) => return e.write_errors().into(),
    };
    let tokens = quote! { #from_model };
    tokens.into()
}
